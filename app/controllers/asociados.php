<?php
require_once 'utils/utils.php';
require_once 'utils/File.php';
require_once 'entity/Asociados.php';

$descripcion='';
$mensaje = '';
$nombre = '';



if ($_SERVER["REQUEST_METHOD"]==="POST") {
  if (empty($_POST["nombre"])) {
    $errores []= "Introduce tu nombre";
  }else {

    try {


      $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

      $nombre = trim(htmlspecialchars($_POST["nombre"]));

      $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

      $imagen = new File("imagen", $tiposAceptados);

      $imagen->saveUploadFile(Asociados::RUTA_IMAGES_LOGOS);

      $mensaje = "Datos enviados";
    } catch (FileException $fileException) {
      $errores []= $fileException->getMessage();
    }
    // code...
  }
}


require __DIR__ .'/../views/asociados.view.php';
 ?>
