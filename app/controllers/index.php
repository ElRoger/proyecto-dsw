<?php
require_once 'utils/utils.php';
require_once 'entity/ImagenGaleria.php';
require_once 'database/Connection.php';
require_once 'entity/Asociados.php';
require_once 'repository/ImagenGaleriaRepository.php';

//$config = require_once("app/config.php");
//guarda   la configuracion en el contenedor de ID
//App::bind("config", $config);
$connection = App::getConnection();
$imagenGaleriaRepository  = new ImagenGaleriaRepository();


$partners = array();

$fotos = $imagenGaleriaRepository->findAll();

for ($i=1; $i <=6 ; $i++) {
  $partners[$i-1]= new Asociados("log".$i.".jpg", "Parcero ".$i);
}


require __DIR__ .'/../views/index.view.php';
?>
