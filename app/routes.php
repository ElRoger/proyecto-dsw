<?php
  return [
   "practicas/A_16/proyecto-dsw/index" => "app/controllers/index.php",

   "practicas/A_16/proyecto-dsw/about" => "app/controllers/about.php",

   "practicas/A_16/proyecto-dsw/asociados" => "app/controllers/asociados.php",

   "practicas/A_16/proyecto-dsw/blog" => "app/controllers/blog.php",

   "practicas/A_16/proyecto-dsw/contact" => "app/controllers/contact.php",

   "practicas/A_16/proyecto-dsw/galeria" => "app/controllers/galeria.php",

   "practicas/A_16/proyecto-dsw/post" => "app/controllers/single_post.php"
  ]
?>
