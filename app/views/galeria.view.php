<?php     //Includes de la cabecera y barra de navegacion
include __DIR__ . "/partials/inicio-doc.partials.php";
include __DIR__ . "/partials/nav.partials.php";
?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr> <!-- Verificamos que nos llegue información de un formularo en el metodo post -->
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
              <!-- Muestra info si mensajes está vacia, si no lo esta imprime danger-->
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <!-- Si no hay errores muestra un mensaje de informacion aceptada -->
                <?php if(empty($errores)) : ?>
                <p><?= $mensaje ?></p>
                <!-- Si hay errores los muestra todos con un foreach -->
                <?php else : ?>
                <ul>
                    <?php foreach($errores as $error) : ?>
                    <li><?= $error ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <form class="form-horizontal"action="<?=$_SERVER["REQUEST_URI"] ?>" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>


                  <label class="label-control">Categoría</label>
                  <select class="form-control" name="categoria">
                   <?php foreach ($categorias as $categoria) : ?>
                   <option value="<?= $categoria->getId()?>"><?= $categoria->getNombre();?></option>
                   <?php endforeach; ?>
                  </select>



                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion" value="<?= $descripcion ?>"></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
          </div>
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Imagen</th>
                  <th>Descripcion</th>
                  <th>Categoria</th>
                  <th>numVisualizaciones</th>
                  <th>numLikes</th>
                  <th>numDownloads</th>
                </tr>
              </thead>
              <tbody>
              <?php
              foreach (($todasLasImagenes ?? []) as $imagen) {
                ?>
                <tr>
                  <td><?= $imagen->getId(); ?></td>
                  <td><img src="<?= ImagenGaleria::RUTA_IMAGENES_GALERIA.$imagen->getNombre()?>"class="img-rounded center-block" id="tamanioImagenes"></td>
                  <td><?= $imagen->getDescripcion(); ?></td>
                  <td><?= $imagenGaleriaRepository->getCategoria($imagen)->getNombre()?> </td>
                  <td><?= $imagen->getNumVisualizaciones(); ?></td>
                  <td><?= $imagen->getNumLikes(); ?></td>
                  <td><?= $imagen->getNumDownloads(); ?></td>
                </tr>

                <?php
              }
               ?>

              </tbody>
            </table>



    </div>
</div>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.partials.php"; ?>
