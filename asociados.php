<?php
require 'utils/utils.php';
require 'utils/File.php';
require 'entity/Asociados.php';

$descripcion='';
$mensaje = '';
$nombre = '';



if ($_SERVER["REQUEST_METHOD"]==="POST") {
  if (empty($_POST["nombre"])) {
    $errores []= "Introduce tu nombre";
  }else {

    try {


      $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

      $nombre = trim(htmlspecialchars($_POST["nombre"]));

      $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

      $imagen = new File("imagen", $tiposAceptados);

      $imagen->saveUploadFile(Asociados::RUTA_IMAGES_LOGOS);

      $mensaje = "Datos enviados";
    } catch (FileException $fileException) {
      $errores []= $fileException->getMessage();
    }
    // code...
  }
}


require 'views/asociados.view.php';
 ?>
