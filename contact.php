<?php
require 'utils/utils.php';
$errores = [];

if ($_SERVER["REQUEST_METHOD"]==="POST") {

  if (empty($_POST["nombre"])) {
    array_push($errores, "Introduce tu nombre");
  }
  if (empty($_POST["apellido"])) {
    array_push($errores, "Introduce tu apellido");
  }
  if (empty($_POST["correo"])) {
    array_push($errores, "Introduce tu correo electronico");

  }else if (filter_var($_POST["correo"], FILTER_VALIDATE_EMAIL)==FALSE) {

    array_push($errores, "Introduce un correo electronico valido");
  }
  if (empty($_POST["tema"]??'')) {
    array_push($errores, "Introduce un tema");
  }
}

require 'views/contact.view.php';
?>
