<?php
/**
 *
 */
class Asociados
{
  protected $nombre;
  protected $logo;
  protected $descripcion;

  const RUTA_IMAGES_LOGOS = "images/index/asociados/";

  function __construct($nombre, $descripcion)
  {
    $this->nombre = $nombre;
    $this->descripcion = $descripcion;
  }

  public function getNombre(){
    return $this->nombre;
  }

  public function getLogo(){
    return $this->logo;
  }

  public function getDescripcion(){
    return $this->descripcion;
  }

  public function setNombre($nombre){
    $this->nombre = $nombre;
    return $this;
  }

  public function setLogo($logo){
    $this->logo = $logo;
    return $this;
  }

  public function setDescripcion($descripcion){
    $this->descripcion = $descripcion;
    return $this;
  }

  public function getURLogo() : string
  {
    return self::RUTA_IMAGES_LOGOS . $this->getNombre();
  }

}
 ?>
