<?php
require_once 'database/IEntity.php';
/**
 *Clase de imagenes, igual a la base de datos
 */
class ImagenGaleria implements IEntity
{
  private $id;

  private $nombre;

  private $descripcion;

  private $numVisualizaciones;

  private $numLikes;

  private $numDownloads;

  private $categoria;

  const RUTA_IMAGENES_PORTFOLIO = "images/index/portfolio/";

  const RUTA_IMAGENES_GALERIA = "images/index/gallery/";

//Constructor
  function __construct($nombre ="", $descripcion="", $categoria=0, $numVisualizaciones=0, $numLikes=0, $numDownloads=0)
  {
    $this->id=null;

    $this->categoria = $categoria;

    $this->nombre=$nombre;

    $this->descripcion=$descripcion;

    $this->numVisualizaciones=$numVisualizaciones;

    $this->numLikes=$numLikes;

    $this->numDownloads=$numDownloads;


  }

  public function toArray(): array
  {
    return [
      "id"                  =>$this->getId(),
      "nombre"              =>$this->getNombre(),
      "descripcion"         =>$this->getDescripcion(),
      "numVisualizaciones"  =>$this->getNumVisualizaciones(),
      "numLikes"            =>$this->getNumLikes(),
      "numDownloads"        =>$this->getNumDownloads(),
      "categoria"           =>$this->getCategoria()
    ];
  }


  public function getURLPortfolio() : string
  {
    return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
  }

  public function getURLGaleria() : string
  {
    return self::RUTA_IMAGENES_GALERIA . $this->getNombre();
  }

    /**
     * Get the value of Nombre
     *
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of Descripcion
     *
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get the value of Num Visualizaciones
     *
     * @return mixed
     */
    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }

    /**
     * Get the value of Num Likes
     *
     * @return mixed
     */
    public function getNumLikes()
    {
        return $this->numLikes;
    }

    /**
     * Get the value of Num Downloads
     *
     * @return mixed
     */
    public function getNumDownloads()
    {
        return $this->numDownloads;
    }


    /**
     * Set the value of Nombre
     *
     * @param mixed nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Set the value of Descripcion
     *
     * @param mixed descripcion
     *
     * @return self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Set the value of Num Visualizaciones
     *
     * @param mixed numVisualizaciones
     *
     * @return self
     */
    public function setNumVisualizaciones($numVisualizaciones)
    {
        $this->numVisualizaciones = $numVisualizaciones;

        return $this;
    }

    /**
     * Set the value of Num Likes
     *
     * @param mixed numLikes
     *
     * @return self
     */
    public function setNumLikes($numLikes)
    {
        $this->numLikes = $numLikes;

        return $this;
    }

    /**
     * Set the value of Num Downloads
     *
     * @param mixed numDownloads
     *
     * @return self
     */
    public function setNumDownloads($numDownloads)
    {
        $this->numDownloads = $numDownloads;

        return $this;
    }


    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get the value of Categoria
     *
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set the value of Categoria
     *
     * @param mixed $categoria
     *
     * @return self
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

}


 ?>
