<?php
require_once 'utils/utils.php';
require_once 'utils/File.php';
require_once 'entity/Categoria.php';
require_once 'entity/ImagenGaleria.php';
require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once 'core/App.php';
require_once 'repository/ImagenGaleriaRepository.php';
require_once 'repository/CategoriaRepository.php';


$descripcion='';
$mensaje = '';



try {
  $config = require_once("app/config.php");
  //guarda   la configuracion en el contenedor de ID
  App::bind("config", $config);
  $connection = App::getConnection();
  $imagenGaleriaRepository  = new ImagenGaleriaRepository();
  $categoriaRepository = new CategoriaRepository();

  if ($_SERVER["REQUEST_METHOD"]==="POST") {

      //Para archivo
      $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

      $categoria = trim(htmlspecialchars($_POST["categoria"]));

      $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

      $imagen = new File("imagen", $tiposAceptados);

      $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALERIA);

      $imagen->
      copyFile(ImagenGaleria::RUTA_IMAGENES_GALERIA,ImagenGaleria::RUTA_IMAGENES_PORTFOLIO );
      //Para base de datos
      //inserta
      $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoria);
      $imagenGaleriaRepository->save($imagenGaleria);
      $mensaje = "Se ha guardado la imagen en la BBDD";

  }
    //consulta para tabla
    $todasLasImagenes = $imagenGaleriaRepository->findAll();

    $categorias = $categoriaRepository->findAll();




  } catch (FileException $fileException) {
    $errores []= $fileException->getMessage();
  }catch (QueryException $QueryException) {
    $errores []= $QueryException->getMessage();
  }catch (AppException $AppException) {
    $errores [] = $AppException->getMessage();
  }



require_once 'views/galeria.view.php';
?>
